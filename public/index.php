<!DOCTYPE html>
<meta charset="utf-8">
<style>
    .node circle {
        fill: #fff;
        stroke: steelblue;
        stroke-width: 2px;
    }

    .node circle[type=daughter]{
        fill: #fff;
        stroke: LightSeaGreen;
        stroke-width: 2px;
    }

    .node {
        font: 10px sans-serif;
    }

    .link {
        fill: none;
        stroke: #ccc;
        stroke-width: 1.5px;
    }
</style>
<body>
<script src="js/d3.js"></script>
<script>

    var width = 1100,
            height = 700;

    var tree = d3.layout.tree()
            .size([height, width - 160]);

    var diagonal = d3.svg.diagonal()
            .projection(function(d) { return [d.y, d.x]; });

    var svg = d3.select("body").append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate(40,0)");

    d3.json("/family.json", function(error, json) {
        if (error) throw error;

        var nodes = tree.nodes(json),
                links = tree.links(nodes);

        var link = svg.selectAll("path.link")
                .data(links)
                .enter().append("path")
                .attr("class", "link")
                .attr("d", diagonal);

        var node = svg.selectAll("g.node")
                .data(nodes)
                .enter().append("g")
                .attr("class", "node")
                .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; })

        node.append("circle")
                .attr("r", 4.5)
                .attr("type", function(d) {
                    return d.type;
                })
                .on("mousedown", function(d) {
                    window.open('/person/'+d.id + ".html");
                })
                .on("mouseover", function() {
                    d3.select(this).attr('transform', 'scale(1.2)');
                })
                .on("mouseout", function() {
                    d3.select(this).attr('transform', '');
                });


        node.append("text")
                .attr("dx", function(d) { return d.children ? -8 : 8; })
                .attr("dy", 3)
                .attr("text-anchor", function(d) { return d.children ? "end" : "start"; })
                .text(function(d) { return d.name; });
    });

    d3.select(self.frameElement).style("height", height + "px");

</script>
